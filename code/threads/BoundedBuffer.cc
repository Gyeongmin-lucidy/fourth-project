
#include "copyright.h"
#include "BoundedBuffer.h"
#include "synch.h"


BoundedBuffer::BoundedBuffer(int c)
{
  lock = new Lock("bounded buffer lock");
  capacity = c;
  itemCount = in = out = 0;

  buffer = new char[capacity];
  empty = new Condition("EMPTY");
  full = new Condition("FULL");
}


BoundedBuffer::~BoundedBuffer()
{ 
  delete buffer;
  delete lock;
  delete empty;
  delete full;
}

//producing method
void BoundedBuffer::Write(char *item)
{
  lock->Acquire();    // acquiring lock : 쓰레드 동작에 대해서 lock을 걸어주는 역할

  //만약 용량을 초과 하였다면 계속 wait 를 걸어주게 된다.
  while (itemCount == capacity){
    cout << "*** item is full ***" << endl;
    full->Wait(lock);
  }

 
  // insert the item into the buffer : 용량에 여유가 있을시에만 insert를 시행
  buffer[in] = *item;
  in = (in + 1) % capacity;
  itemCount++;


  printf("*** %s added item no.%c   ", kernel->currentThread->getName(), *item);
  printf("current : %d \n", itemCount);
  empty->Signal(lock);    //empty condition에 대해서 현재 item이 채워졌음을 알려줌
  lock->Release();        //releasing lock : lock을 해제함으로 다른 쓰레드에대해 write 가능
}

//consuming method
void BoundedBuffer::Read()
{
  char *item;

  lock->Acquire(); // acquiring lock : 쓰레드 동작에 대해서 lock을 걸어주는 역할
  //만약 consuming 할수 있는 item이 없다면 계속 wait 를 걸어주게 된다.
  while(itemCount == 0){
    cout << "*** item is empty ***"<<endl;
    empty->Wait(lock);
  }

  // Consume an item from the buffer : 소비할 item이 있을시에만 consume실행
  item = &buffer[out];
  out = (out + 1) % capacity;
  itemCount--;

  printf("*** %s took  item no.%c   ", kernel->currentThread->getName(), *item);
  printf("current : %d \n", itemCount);

  full->Signal(lock);   //full condition에 대해서 현재 item이 소비되었음을 알려줌
  lock->Release();      //releasing lock : lock을 해제함으로 다른 쓰레드에대해 read 가능
}
