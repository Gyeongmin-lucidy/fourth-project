// Header file for BoundedBuffer.cc (see that file
// for documentation).

#include "list.h"
#include "synch.h"

class BoundedBuffer {
  public:
    BoundedBuffer(int c);	//constructor -> parameter : capacity
    ~BoundedBuffer();     //destructor -> free to dynamic poiner

    void Write(char *item); //producer -> item add method
    void Read();            //consumer -> item consume method

  private:
    char* buffer;       // bounded buffer -> meaning size of buffer
    int in, out;        // variable about in, out
    int capacity;       // capacity of the bounded buffer.
    int itemCount;     // total items in the bounded buffer
    Lock *lock;
    Condition *empty;  // wait in Remove if the list is empty
    Condition *full;   // wait in Append if the list is FULL.
};
